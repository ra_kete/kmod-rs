use core::fmt::{self, Write};
use crate::ffi::{CStr, CString};

pub struct KernelLog {}

impl Write for KernelLog {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        let cs = match CString::new(s) {
            Ok(cs) => cs,
            Err(_) => return Err(fmt::Error),
        };

        printk(&cs);
        Ok(())
    }
}

pub fn print(args: fmt::Arguments) {
    let mut w = KernelLog {};
    w.write_fmt(args)
        .unwrap_or_else(|e| panic!("failed printing to kernel log: {}", e));
}

fn printk(s: &CStr) -> i32 {
    unsafe { linux_sys::printk(s.as_ptr()) }
}
