#![no_std]
#![feature(alloc)]
#![feature(alloc_error_handler)]

extern crate alloc;

pub mod ffi;
pub mod log;

mod allocator;
mod macros;
mod panic;

use crate::allocator::LinuxAllocator;

#[global_allocator]
pub static A: LinuxAllocator = LinuxAllocator;
